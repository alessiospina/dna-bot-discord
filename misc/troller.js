const fs = require('fs');
const path = require('path');


module.exports = class Troller {
    
    constructor() {
        let rawdata = fs.readFileSync(path.join(__dirname, "quotes.json"));
        this.quotes = JSON.parse(rawdata);
    }

    extractRandomQuote() {
        const randomIndex = Math.floor(Math.random() * this.quotes.length);   
        return this.quotes[randomIndex].quote
    }
}