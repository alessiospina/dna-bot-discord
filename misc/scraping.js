// scraping framework
const cheerio = require('cheerio')

// request framework to external ip
const request = require('request');



module.exports = class Scraping {

    constructor(link) {
        this.link = link
    }

    getPlayers() {
        return new Promise((resolve, reject) => {
            request(this.link, function (error, response, body) {

                if(error != null)
                    reject(error)
                
                const $ = cheerio.load(body);
    
                $('li').each(function(i){
                    
                    const component = $(this).text().trim()
                    // i = 26 ===> counter of players
                    if(i == 26) {
                        // remove special characters
                        const count = component.replace(/[^Z0-9]/g, "")
    
                        // bot response
                        resolve(count)
                    }
                })
            });
        })
    }
}