
const Scraping = require('./scraping')
const Routine = require('./routine')
const Chart = require('./chart')

module.exports = class Controller {

    constructor(link,timeMillisecs){
        this.scrapingService = new Scraping(link)
        this.routineService = new Routine(timeMillisecs,this.scrapingService)
        this.chartService = new Chart()

        this.routineService.startRoutine()
        //this.chartService.buildingChart()
    }
    

    countPlayers() {
        return new Promise((resolve, reject) => {
            
            this.scrapingService.getPlayers().then((count) => {
                
                var message = " Attualmente ci sono: " + count + " players!"
                
                if(count < 10) 
                    message = message + '\nPrevisione: **FALLIMENTO TOTALE**'
                else if(count >= 10 && count < 25)
                    message = message + '\nPrevisione: **Il Declino**'
                else if (count >= 25 && count <= 40)
                    message = message + '\nPrevisione: **Probabile Declino**'
                else if( count > 40 && count <= 60)
                    message = message + '\nPrevisione: **Probabile Risalita**'
                else 
                    message = message + '\nPrevisione: **La Risalita**'

                resolve(message)
            }).catch(error => {
                reject(error)
            })
        }) 
    }


    totalChart() {

    }




    commandsInfo() {
        return "\n**Hey, questi sono i comandi disponibili:**" + "\n\n" +
                "1) **!dnabot cp**\tmostra il numero di player" + "\n" +
                "2) **!dnabot total-chart**\tmostra l'andamento grafico dei players con tutti i dati raccolti dal sistema" + "\n"
    }

}