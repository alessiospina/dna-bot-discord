
const fs = require("fs")

module.exports = class Logger {
    
    constructor(path) {
        this.path = path
    }

    save(author, message) {
        const date = new Date()
        const stringToSave = "[" + date + "] <"+ author + "> :" + " " + message + "\n"
        console.log(stringToSave)
        fs.appendFileSync(this.path, stringToSave)
    }
}