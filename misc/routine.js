
const schedule = require('node-schedule')
const Record = require('../models').Record

module.exports = class Routine {

    constructor(time,scrapingService) {
        this.time = time
        this.scrapingService = scrapingService
    }

    startRoutine() {
        
        const _this = this

        setInterval(function() {

            _this.scrapingService.getPlayers().then(count => {
                
                if(count == null)
                    return

                Record.create({ date: new Date(), count: count })
                    .then(entityCreated => {
                        console.log('Record id: ' + entityCreated.id + " created")})
                    .catch(error => {
                        console.log('startRoutine ERROR: ' + error)
                })
                
            }).catch(error => {
                console.log('Error: ' + error)
            })     
        }, this.time)
    }
}