"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var fs = require('fs');

var path = require('path');

module.exports =
/*#__PURE__*/
function () {
  function Troller() {
    _classCallCheck(this, Troller);

    var rawdata = fs.readFileSync(path.join(__dirname, "quotes.json"));
    this.quotes = JSON.parse(rawdata);
  }

  _createClass(Troller, [{
    key: "extractRandomQuote",
    value: function extractRandomQuote() {
      var randomIndex = Math.floor(Math.random() * this.quotes.length);
      return this.quotes[randomIndex].quote;
    }
  }]);

  return Troller;
}();