'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    discordID: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    username: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName:'USER',
    paranoid: false
  });
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};
