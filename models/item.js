'use strict';
module.exports = (sequelize, DataTypes) => {
  const Item = sequelize.define('Item', {
    user_id: DataTypes.STRING,
    item_name: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName:'ITEM',
    paranoid: false
  });
  Item.associate = function(models) {
    // associations can be defined here
  };
  return Item;
};
