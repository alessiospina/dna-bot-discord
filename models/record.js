'use strict';
module.exports = (sequelize, DataTypes) => {
  const Record = sequelize.define('Record', {
    date: DataTypes.DATE,
    count: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName:'RECORD',
    paranoid: false
  });
  Record.associate = function(models) {
    // associations can be defined here
  };
  return Record;
};

// sequelize model:create --name Record --attributes date:date,count:integer
