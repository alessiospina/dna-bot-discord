"use strict";

var Discord = require('discord.js');

var bot = new Discord.Client();

var Controller = require('./misc/controller');

var time = 3 * 3600000;
var controllerService = new Controller('https://gasce.com/server/273', time);

var Troller = require('./misc/troller');

var trollerService = new Troller();
var token = 'NzEyMjcwMTY5NDUxNTI4MjEz.XsPLPg.39SXqQwfHsaqIGWzTSXCwwNdYhM';
var prefix = '!dnabot '; //https://git.heroku.com/dna-bot-discord.git

console.log("\n\n\n\========== [DNA-BOT] =============\n");

var models = require("./models");

models.sequelize.sync({
  force: false
}).then(function () {
  console.log("Database Connected successfully");
}); // startup

bot.on('ready', function () {
  console.log("DNA BOT connected...");
}); // for each message received

bot.on('message', function (msg) {
  var content = msg.content; //console.log("[DNA-BOT] Message Received: " + content)
  //console.log(content.slice(0,(prefix.length)))
  //getting prefix !dnabot

  var prefixMessage = content.slice(0, prefix.length); // if message contains !dnabot as prefix

  if (prefixMessage == prefix) {
    var suffix = content.slice(prefix.length, content.length);

    if (suffix == "cp") {
      controllerService.countPlayers().then(function (message) {
        console.log('[command: cp] called by: ' + msg.author.username);
        msg.reply(message);
      });
    } else if (suffix == "total-chart") {
      //controllerService
      msg.reply("**Arriverà presto... fallito**");
    } else if (suffix == "help") {
      msg.reply(controllerService.commandsInfo());
    } else if (suffix == "status") {
      var prevision = trollerService.extractRandomQuote();
      msg.reply("**Status attuale**: => *" + prevision + "*");
    }
  }
});
bot.login(token);