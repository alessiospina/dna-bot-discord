
const path = require('path'); 

const Discord = require('discord.js');
const bot = new Discord.Client();


const Controller = require('./misc/controller')
const time = 3 * 3600000
const controllerService = new Controller('https://gasce.com/server/273', time)

const Troller = require('./misc/troller')
const trollerService = new Troller()

const Logger = require('./misc/logger')

const pathstring = path.join(__dirname,"logs","logs.txt")
const logger = new Logger(pathstring)

const token = 'NzEyMjcwMTY5NDUxNTI4MjEz.XsPLPg.39SXqQwfHsaqIGWzTSXCwwNdYhM'
const prefix = '!dnabot ' 

//https://git.heroku.com/dna-bot-discord.git

console.log("\n\n\n\========== [DNA-BOT] =============\n")


var models = require("./models");

models.sequelize
  .sync({ force: false })
  .then(function() {
    console.log("Database Connected successfully");
  })



// startup
bot.on('ready', () => {
    console.log("DNA BOT connected...")
})


// for each message received
bot.on('message', (msg) => {
    
    const content = msg.content
    const author = msg.author.username
    
    //logger.save(author,content)

    //console.log("[DNA-BOT] Message Received: " + content)
    //console.log(content.slice(0,(prefix.length)))

    //getting prefix !dnabot
    const prefixMessage = content.slice(0,(prefix.length))


    // if message contains !dnabot as prefix
    if(prefixMessage == prefix) {
        
        const suffix = content.slice(prefix.length, content.length);
        
        if(suffix == "cp") { 
            controllerService.countPlayers().then(message => {
                console.log('[command: cp] called by: ' + msg.author.username)
                msg.reply(message)
            })
        }
        else if (suffix == "total-chart") {
            //controllerService
            msg.reply("**Arriverà presto... fallito**")
        }
        else if(suffix == "help") {
            msg.reply(controllerService.commandsInfo())
        }
        else if(suffix == "status") {
            const prevision = trollerService.extractRandomQuote()
            msg.reply("**Status attuale**: => *" + prevision + "*")
        }
    }
})


bot.login(token);

